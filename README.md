# quick fly

Easy script for searching cheapest return flight(s) from/to more destinations.
It uses API from kiwi.com web pages.


## prerequisite

Register new account on [kiwi](http://tequila.kiwi.com) API page. Few steps:
Create new 'Solution' / 'Meta Search  API Integration' / 'One-way and Return'.
Then name could be any string (eg: 'quickfly'). Afterwards open that solution 
in 'My Solutions' menu on the left. Check for 'API key:' in 'Details' section.
Copy the API key and put it in terminal variable named 'KIWI\_API' macOS/Linux:

```bash
export KIWI_API=<your_key>
```

For Windows open 'cmd' command and:
```bash
set KIWI_API=<your_key>
```

## usage

In search-my-flight.py you can change IATA codes for airports from/to:
These 2 variables are self explaining. Each code separate by coma.  
my\_from = "BER,LTN"  
my\_to = "AGP,BKK"  


With KIWI\_API variable set and in the directory where you download/clone run:

```bash
python3 ./search-my-flight.py
```

### example of result(s):

From: Berlin, To: Skopje, depart: 2023-09-06 nights: 16, price: 72 EUR  
From: Roma, To: Paris, depart: 2023-09-12 nights: 3, price: 77 EUR  
...

