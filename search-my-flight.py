"""
    This short script requests cheapest flights from Kiwi pages.

    It can consume various Start airports to many destinations - IATA code(s).
    Result is written each flight on single line:

        From: Berlin, To: Skopje, depart: 2023-09-06 nights: 16, price: 72 EUR
        From: Roma, To: Paris, depart: 2023-09-12 nights: 3, price: 77 EUR

    Don't forget export KIWI_API variable in terminal before running script !
    Linux/macOS
    export KIWI_API=<your_tequila_api_code>
    Windows
    set KIWI_API=<your_tequila_api_code>
"""

import requests
import os
from datetime import timedelta, date

# from which airports to destinations; string divider is coma ("BER,DTM,LTN")
my_from = "VIE,BTS"
my_to = "AGP,LPA,BKK,KUL"

# kiwi api from https://tequila.kiwi.com
kiwi_url = "https://api.tequila.kiwi.com/v2/search"
kiwi_api = os.environ.get("KIWI_API")

# import apikey from env variable
# in terminal:  export KIWI_API=<your_api_code>
header = {
     "apikey": kiwi_api
}

# flight definitions; for more details you can visit Tequila's DOC pages
my_flight = {
    "locale": "en-US",
    "location_types": "airport",
    "limit": 100,
    "nights_in_dst_from": 4,
    "nights_in_dst_to": 70,
    "ret_from_diff_city": False,
    "ret_to_diff_city": False,
    "one_for_city": 1,
    "selected_cabins": "M",
    "curr": "EUR"
}

# select days when flying
day_from = date.strftime(date.today() + timedelta(days=4), "%d/%m/%Y")
day_to = date.strftime(date.today() + timedelta(days=12), "%d/%m/%Y")
return_from = date.strftime(date.today() + timedelta(days=5), "%d/%m/%Y")
return_to = date.strftime(date.today() + timedelta(days=60), "%d/%m/%Y")


# preparing request's parameters from variables
my_flight['fly_from'] = my_from
my_flight['fly_to'] = my_to
my_flight['date_from'] = day_from
my_flight['date_to'] = day_to
my_flight['return_from'] = return_from
my_flight['return_to'] = return_to

# kiwi input criteria (for own debug purposes)
# print(my_flight)

# calling api with desired params;
r = requests.get(url=kiwi_url, params=my_flight, headers=header)
r.raise_for_status()
f_data = r.json()["data"]

# go over all results and filling variables; printing final result(s)
for rec in range(len(f_data)):
    my_curr = r.json()["currency"]
    city_from = r.json()["data"][rec]["cityFrom"]
    city_to = r.json()["data"][rec]["cityTo"]
    local_dep = r.json()["data"][rec]["local_departure"][:10]
    nights = r.json()["data"][rec]["nightsInDest"]
    price = r.json()["data"][rec]["price"]
    print(f"From: {city_from}, To: {city_to}, depart: {local_dep}"
          f" nights: {nights}, price: {price} {my_curr}")
